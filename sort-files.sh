#!/bin/bash

# Brighness Program should give the brightness value number (expectedly integer)
BRIGHTNESS_PROGRAM=/home/$USER/.local/little_applications/bin/brightnessextractor
# Shape Program should answer name of the shape category the image should go into
## This name needs to be a viable filename
## e.g. vertical horizontal small
SHAPE_PROGRAM=/home/$USER/.local/little_applications/bin/ImageDataReader

HELP_INFO="Usage :\n
\t	sort-files.sh [OPTIONS] Input-Directory Output-Directory\n
OPTIONS:\n
\t	--brightness-separator <brightness-value>\n
\t	\t	brightness-value is expected to be in [0, 100]\n
\t	\t	This might be different if your brightness program returns a different range\n
\t	--brightness-program <program-full-path>\n
\t	\t	Default: $BRIGHTNESS_PROGRAM\n"

# OPTIONS
if [ -z "$1" ]; then
	echo -e $HELP_INFO
	exit
else
	while [ -n "$1" ]
	do
		case $1 in

		"--brightness-separator")
			shift
			if [ -z $1 ]; then
				echo "Value required for --brightness-separator"
				exit
			fi
			BRIGHTNESS_VALUE=$1
			shift
		;;

		"--brightness-program")
			shift
			if [ -z $1 ]; then
				echo "Value required for --brightness-program"
				exit
			fi
			BRIGHTNESS_PROGRAM=$1
			shift
		;;

		"--help" | "-h")
			echo -e $HELP_INFO
			exit
		;;

		"-" | "--")
			echo "No Implementation for - or --"
			exit
		;;

		"--writing-on-root")
			WRITING_ON_ROOT=true
			shift
		;;

		"--take-dir-from-env")
			shift
		;;

		*)
			INPUT_DIRECTORY=$1
			shift
			if [ -n "$1" ]; then
				OUTPUT_DIRECTORY=$1
			fi
			shift
# 			echo "IN: "$INPUT_DIRECTORY "OUT: "$OUTPUT_DIRECTORY
		;;
		esac
	done
fi

if [ -z "$INPUT_DIRECTORY" ]; then
	echo "Required Argument: Input-Directory"
	echo -e $HELP_INFO
	exit
fi

if [ -z "$OUTPUT_DIRECTORY" ]; then
	echo "Required Argument: Output-Directory"
	echo -e $HELP_INFO
	exit
fi

if [ "$OUTPUT_DIRECTORY" = "/" ]; then
	if [ -z $WRITING_ON_ROOT ]; then
		echo -e "Sanity check. Your output directory is $OUTPUT_DIRECTORY\n\t Please use the --writing-on-root option if you actually intended on doing this."
		exit
	fi
fi

# Resolve the ~
if [ ${INPUT_DIRECTORY:0:1} = '~' ]; then
	INPUT_DIRECTORY="/home/$USER/${INPUT_DIRECTORY:1}"
fi
if [ ${OUTPUT_DIRECTORY:0:1} = '~' ]; then
	OUTPUT_DIRECTORY="/home/$USER/${OUTPUT_DIRECTORY:1}"
fi

if [ ! -d "$INPUT_DIRECTORY" ]; then
	echo "Invalid Input Directory"
	exit
fi

if [ -n "$BRIGHTNESS_VALUE" ]; then
	for file in "$INPUT_DIRECTORY"/*;
	do if [ -f "$file" ]; then
		echo $file
		echo $file 1>&2
		BRIGHTNESS_TARGET=
		SHAPE_TARGET=
		# Get Shape
		SHAPE_TARGET=$(identify -verbose -ping "$file" | $SHAPE_PROGRAM)
		# Get Brightness
		IMAGE_BRIGHTNESS=$(convert "$file" -colorspace hsb -resize 1x1 txt:- | $BRIGHTNESS_PROGRAM)
		if [ -n "$IMAGE_BRIGHTNESS" ]; then
			if [ $IMAGE_BRIGHTNESS -lt $BRIGHTNESS_VALUE ]; then
				BRIGHTNESS_TARGET="dark"
			else
				BRIGHTNESS_TARGET="light"
			fi
		fi
		# Output File
		if [ -n "$SHAPE_TARGET" ]; then
			echo "$OUTPUT_DIRECTORY/$BRIGHTNESS_TARGET/$SHAPE_TARGET/"
			mkdir -p "$OUTPUT_DIRECTORY/$BRIGHTNESS_TARGET/$SHAPE_TARGET/"
			out_file="${file##*/}"
			cp "$file" "$OUTPUT_DIRECTORY/$BRIGHTNESS_TARGET/$SHAPE_TARGET/$out_file"
		else
			echo "Not Used"
		fi
		echo "__________________________________________________"
		echo "__________________________________________________" 1>&2
	fi done
else
	for file in "$INPUT_DIRECTORY"/*;
	do if [ -f "$file" ]; then
		echo $file
		echo $file 1>&2
		SHAPE_TARGET=
		# Get Shape
		SHAPE_TARGET=$(identify -verbose -ping "$file" | $SHAPE_PROGRAM)
		if [ -n "$SHAPE_TARGET" ]; then
			echo "$OUTPUT_DIRECTORY/$SHAPE_TARGET/"
			mkdir -p "$OUTPUT_DIRECTORY/$SHAPE_TARGET/"
			out_file="${file##*/}"
			cp "$file" "$OUTPUT_DIRECTORY/$SHAPE_TARGET/$out_file"
		else
			echo "Not Used"
		fi
		echo "__________________________________________________"
		echo "__________________________________________________" 1>&2
	fi done
fi
